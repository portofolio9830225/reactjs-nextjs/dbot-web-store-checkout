import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import { setCartItem } from "../../store/actions/cartAction";
import isEmpty from "../../utils/isEmpty";
import { withWidth, withMobileDialog, Typography, Grow, Zoom, Fade, Collapse } from "@material-ui/core";

import { AddRounded, DeleteOutlineRounded, RemoveRounded } from "@material-ui/icons";
import { black, errorColor, greydark } from "../../utils/ColorPicker";

const styles = theme => ({
	image: {
		width: "100%",
		height: "100%",
		objectFit: "cover",
	},

	productCard: {
		height: "100px",
		marginBottom: "2.5rem",
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		borderRadius: "15px",
		boxShadow: "3px 3px 15px #d6d6e3, -4px -4px 20px #ffffff",
		overflow: "hidden",
		background: "linear-gradient(309.34deg, #F2F3F6 -13.68%, #E5E6EC 171.92%)",
		// backgroundColor: mainBgColor,
		[theme.breakpoints.down("xs")]: {
			height: "80px",
		},
	},
	productCardImgContainer: {
		position: "relative",
		width: "100px",
		height: "100px",
		[theme.breakpoints.down("xs")]: {
			width: "80px",
			height: "80px",
		},
	},
	productCardTextContainer: {
		boxSizing: "border-box",
		display: "flex",
		flexDirection: "column",
	},
	productCardActionContainer: {
		marginTop: "2.5rem",
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		[theme.breakpoints.down("xs")]: {
			marginTop: "1.5rem",
		},
	},
	title: {
		fontSize: "1.2rem",
		fontWeight: 500,
		letterSpacing: "0.5px",
		width: "100%",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	lDescText: {
		fontWeight: 300,
		fontSize: "1rem",
		letterSpacing: "0.3px",
		color: greydark,
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.9rem",
		},
	},
	price: {
		fontWeight: 500,
		color: greydark,
		marginTop: "0.5rem",
		fontSize: "1.1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},

	addIconContainer: {
		transition: "all 0.3s",
		cursor: "pointer",
		boxShadow:
			"-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		// boxShadow:
		//   "-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px #e3eeff, inset -2px -2px 4px rgba(0, 101, 255, 0.1), inset 2px 2px 4px #FFFFFF",
		// boxShadow: "2px 2px 4px #e3eeff, -2px -2px 4px #ffffff",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		marginRight: "10px",
		background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		"&:active": {
			background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		},
		borderRadius: "50%",
		height: "50px",
		width: "50px",
		[theme.breakpoints.down("xs")]: {
			height: "35px",
			width: "35px",
		},
	},
	addIcon: {
		transition: "all 0.3s",
		color: greydark,
		fontWeight: "bold",
		fontSize: "1.8rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.3rem",
		},
	},

	counterClickerContainer: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
		height: "100%",
	},

	counterText: {
		color: greydark,
		fontWeight: "bold",
		textShadow: "1px 1px 2px #999, -1px -1px 2px #ffffff",
	},

	counterIconContainer: {
		transition: "all 0.3s",
		cursor: "pointer",
		boxShadow:
			"-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",

		background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		"&:active": {
			background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		},
		height: "40px",
		width: "40px",
		borderRadius: "20px",
		[theme.breakpoints.down("xs")]: {
			height: "32px",
			width: "32px",
			borderRadius: "16px",
		},
	},
	counterIcon: {
		color: greydark,
		fontWeight: "bold",
		fontSize: "1.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.2rem",
		},
	},
	maxLimitText: {
		fontWeight: 500,
		color: errorColor,
		textAlign: "center",
		letterSpacing: "0.1px",
		fontSize: "0.8rem",
		paddingTop: "0.2rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.65rem",
			paddingTop: "0.1rem",
		},
	},
});

class ProductBox extends Component {
	state = {
		maxLimit: null,
		counter: this.props.counter,
		isCounterInactive: isEmpty(this.props.onClick),
		isCounterExpand: false,
	};
	counterExpandController = null;

	componentDidMount() {
		let itmNum = this.props.stock;
		if (!isEmpty(this.props.max) && this.props.stock > this.props.max) {
			itmNum = this.props.max;
		}
		this.setState({
			maxLimit: itmNum,
		});
		if (!this.state.isCounterInactive) {
			this.updateCartCount();
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.cart.list !== this.props.cart.list) {
			this.updateCartCount();
		}
	}

	updateCartCount = () => {
		let foundInCart = this.props.cart.list.find(f => f.item_id === this.props.itmID);

		this.setState({
			counter: !isEmpty(foundInCart) ? foundInCart.quantity : 0,
		});
	};

	handleCounter = action => () => {
		clearTimeout(this.counterExpandController);
		let isClosed = false;
		let prevCounterAction = this.state.isCounterExpand;
		let prevCounter = this.state.counter;
		let numDecrease = this.state.counter - 1;
		let numIncrease = this.state.counter + 1;

		this.setState(
			{
				isCounterExpand: true,
			},
			() => {
				switch (action) {
					case "-":
						{
							this.setState(
								{
									counter: this.state.counter > 0 ? numDecrease : 0,
								},
								() => {
									if (prevCounter !== this.state.counter) {
										this.props.setCartItem(
											this.props.cart.list,

											this.props.business.detail.business_id,
											{
												item_id: this.props.itmID,
												quantity: this.state.counter,
											}
										);
									}
									if (this.state.counter <= 0) {
										isClosed = true;
										this.setState({
											isCounterExpand: false,
										});
									}
								}
							);
						}
						break;
					case "+": {
						this.setState(
							{
								counter:
									(prevCounterAction || this.state.counter <= 0) && numIncrease <= this.state.maxLimit
										? numIncrease
										: this.state.counter,
							},
							() => {
								if (prevCounter !== this.state.counter) {
									this.props.setCartItem(
										this.props.cart.list,
										this.props.business.detail.business_id,
										{
											item_id: this.props.itmID,
											quantity: this.state.counter,
										}
									);
								}
							}
						);
					}
				}
			}
		);

		if (!isClosed) {
			this.counterExpandController = setTimeout(() => {
				this.setState({ isCounterExpand: false });
			}, 2500);
		}
	};

	render() {
		const { width, classes, title, desc, img, price, onClick, stock, max } = this.props;
		const { counter, isCounterInactive, isCounterExpand, maxLimit } = this.state;
		const isMobile = width === "xs";
		const clickableWidth = isMobile ? "calc(100% - 90px)" : "calc(100% - 110px)";
		const isCounterHide = stock <= 0 || (!isEmpty(max) && max <= 0) || isCounterInactive;

		const SoldOutTag = () => {
			return (
				<div
					style={{
						width: "100%",
						height: "100%",
						position: "absolute",
						top: 0,
						left: 0,
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<div
						style={{
							width: "70px",
							height: "70px",
							borderRadius: "35px",
							backgroundColor: "rgba(0,0,0,0.4)",
							display: "flex",
							alignItems: "center",
							justifyContent: "center",
						}}
					>
						<Typography
							style={{
								color: "white",
								fontSize: "0.8rem",
								letterSpacing: "0.5px",
							}}
						>
							Sold Out
						</Typography>
					</div>
				</div>
			);
		};

		return (
			<div className={classes.productCard} style={{ cursor: "pointer" }}>
				<div
					style={{
						display: "flex",
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between",
						transition: "all 0.3s",
						height: "100%",
						width: isCounterHide
							? "95%"
							: `calc(100% - ${
									isCounterExpand ? (isMobile ? "105px" : "140px") : isMobile ? "60px" : "75px"
							  })`,
					}}
					onClick={
						isCounterExpand
							? () => {
									this.setState(
										{
											isCounterExpand: false,
										},
										() => {
											clearTimeout(this.counterExpandController);
										}
									);
							  }
							: onClick
					}
				>
					<div className={classes.productCardImgContainer}>
						{stock <= 0 && <SoldOutTag />}
						<img src={img} className={classes.image} />
					</div>
					<div className={classes.productCardTextContainer} style={{ width: clickableWidth }}>
						<Typography noWrap className={classes.title} style={{ color: stock <= 0 ? greydark : black }}>
							{title}
						</Typography>
						{!isEmpty(desc) && (
							<Typography noWrap className={classes.lDescText}>
								{desc}
							</Typography>
						)}
						<div
							style={{
								width: "100%",
								display: "flex",
								flexDirection: "row",
								justifyContent: "space-between",
							}}
						>
							{!isEmpty(price) && <Typography className={classes.price}>RM{price.toFixed(2)}</Typography>}
							{isCounterInactive && (
								<Typography className={classes.price} style={{ fontWeight: 700 }}>
									x {counter}
								</Typography>
							)}
						</div>

						{/* <Typography
            className={classes.stockText}
            style={{ fontWeight: 400 }}
          >
            Limit to {limit} item{limit > 1 ? "s" : ""}
          </Typography>
          <Typography className={classes.stockText}>Stock: {stock}</Typography> */}
					</div>
				</div>

				{isCounterHide ? null : (
					<div
						className={classes.counterClickerContainer}
						style={{
							width: isCounterExpand ? (isMobile ? "105px" : "140px") : isMobile ? "60px" : "75px",
						}}
					>
						<div
							style={{
								transition: "all 0.25s",
								display: "flex",
								flexDirection: "row",
								boxSizing: "border-box",
								alignItems: "center",
								padding: isCounterExpand ? "0.3rem" : 0,
								justifyContent: isCounterExpand ? "space-between" : "center",
								boxShadow: isCounterExpand
									? "inset 2px 2px 4px #d6d6e3, inset -2px -2px 4px #ffffff"
									: "none",
								width: isCounterExpand ? (isMobile ? "90px" : "130px") : isMobile ? "38px" : "46px",
								height: isCounterExpand
									? isMobile
										? "calc(30px + 0.6rem)"
										: "calc(40px + 0.8rem)"
									: isMobile
									? "38px"
									: "46px",
								borderRadius: isCounterExpand
									? isMobile
										? "calc(15px + 0.3rem)"
										: "calc(20px + 0.4rem)"
									: isMobile
									? "19px"
									: "23px",
								border: isCounterExpand ? "none" : counter <= 0 ? "none" : "3px solid #25d366",
							}}
						>
							<Zoom in={isCounterExpand} mountOnEnter unmountOnExit>
								<div className={classes.counterIconContainer} onClick={this.handleCounter("-")}>
									{counter <= 1 && (
										<Zoom in={counter <= 1}>
											<DeleteOutlineRounded className={classes.counterIcon} />
										</Zoom>
									)}

									{counter > 1 && (
										<Zoom in={counter > 1}>
											<RemoveRounded className={classes.counterIcon} />
										</Zoom>
									)}
								</div>
							</Zoom>
							<Grow in={isCounterExpand} mountOnEnter unmountOnExit>
								<Typography
									className={classes.counterText}
									style={{ fontSize: isMobile ? "0.9rem" : "1.2rem" }}
								>
									{counter}
								</Typography>
							</Grow>
							<div
								className={classes.counterIconContainer}
								style={{
									boxShadow: counter > 0 && !isCounterExpand && "none",
								}}
								onClick={this.handleCounter("+")}
							>
								{counter <= 0 || isCounterExpand ? (
									<Fade in={counter <= 0 || isCounterExpand} mountOnEnter unmountOnExit>
										<div
											style={{
												width: "100%",
												height: "100%",
												display: "flex",
												justifyContent: "center",
												alignItems: "center",
												alignSelf: "center",
											}}
											// onClick={this.handleCounter("+")}
										>
											<AddRounded className={classes.counterIcon} />
										</div>
									</Fade>
								) : (
									<Fade in={!(counter <= 0 || isCounterExpand)} mountOnEnter unmountOnExit>
										<Typography
											style={{
												fontSize: isMobile ? "0.9rem" : "1.2rem",
												fontWeight: "bold",
												textShadow: "1px 1px 2px #999, -1px -1px 2px #ffffff",
												color: greydark,
											}}
										>
											{counter}
										</Typography>
									</Fade>
								)}
							</div>
						</div>
						<Collapse in={counter >= maxLimit && isCounterExpand}>
							<Typography className={classes.maxLimitText}>Max. quantity reached</Typography>
						</Collapse>
					</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	business: state.business,
	cart: state.cart,
	loading: state.loading,
	notification: state.notification,
	error: state.error,
});

export default connect(mapStateToProps, { setCartItem })(
	withStyles(styles)(withMobileDialog({ breakpoint: "xs" })(withRouter(withWidth()(ProductBox))))
);
