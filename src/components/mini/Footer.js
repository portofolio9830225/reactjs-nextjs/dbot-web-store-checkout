import React, { Component } from "react";
import { withRouter } from "next/router";

import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const styles = (theme) => ({
  root: {
    zIndex: 50,
    boxSizing: "border-box",
    width: "100%",
    padding: "4rem 1.5rem 1rem",
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("xs")]: {
      padding: "2rem 0.5rem 0.5rem",
      flexDirection: "column-reverse",
      alignItems: "center",
    },
  },
  textContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",

    [theme.breakpoints.down("xs")]: {
      width: "90%",
      marginTop: "0.7rem",
      justifyContent: "space-between",
    },
  },
  linkContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },

  linkText: {
    letterSpacing: "0.7px",
    fontSize: "1.1rem",
    cursor: "pointer",
    padding: "0.5rem 0 0.5rem 1rem",
    margin: "0.2rem",
    textDecoration: "underline",
    [theme.breakpoints.down("sm")]: {
      margin: "0.1rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
  text: {
    letterSpacing: "0.7px",
    fontSize: "1.1rem",
    margin: "0.2rem",
    //color: "#888",
    [theme.breakpoints.down("sm")]: {
      margin: "0.1rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
  linkIcon: {
    marginBottom: "0.5rem",
    paddingLeft: "1rem",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    cursor: "pointer",
    color: "black",
    [theme.breakpoints.down("xs")]: {
      padding: "0 0.5rem",
      marginBottom: 0,
    },
  },
  sectionTitle: {
    //color: "#888",
    letterSpacing: "1px",
    fontSize: "1.3rem",
    marginBottom: "0.7rem",
  },
  subsection: {
    width: "30%",
    display: "flex",
    flexDirection: "column",
    minWidth: "200px",
    marginBottom: "3rem",
  },
  social: {
    display: "flex",
    flexDirection: "row",
    [theme.breakpoints.down("xs")]: {
      width: "90%",
      justifyContent: "space-between",
    },
  },
});

class Footer extends Component {
  handlePage = (name) => () => {
    if (link.includes("http")) {
      window.open(link);
    } else {
      this.props.router.push(name);
    }
  };

  handleSocial = (url) => () => {
    window.open(url);
  };

  render() {
    const { classes, bgColor, position } = this.props;

    const LinkText = (props) => (
      <Typography
        className={classes.linkText}
        onClick={this.handlePage(props.page)}
      >
        {props.children}
      </Typography>
    );

    const LinkIcon = (props) => (
      <div
        style={props.style}
        onClick={this.handleSocial(props.link)}
        className={classes.linkIcon}
      >
        <i className={props.iconName} />
      </div>
    );

    return (
      <div
        className={classes.root}
        style={{
          backgroundColor: bgColor ? bgColor : "#fff",
          position: position ? position : "relative",
          bottom: position ? 0 : null,
        }}
      >
        <div className={classes.textContainer}>
          <Typography className={classes.text}>
            2020 Inonity Sdn. Bhd.
          </Typography>
          <div className={classes.linkContainer}>
            <LinkText page="/privacy">Privacy</LinkText>
            <LinkText page="/terms">Terms</LinkText>
          </div>
        </div>
        <div className={classes.social}>
          <LinkIcon
            iconName="icon ion-logo-facebook"
            link="https://to.pinjam.co/facebook"
          />
          <LinkIcon
            iconName="icon ion-logo-linkedin"
            link="https://to.pinjam.co/linkedin"
          />
          <LinkIcon
            iconName="icon ion-logo-twitter"
            link="https://to.pinjam.co/twitter"
          />
          <LinkIcon
            iconName="icon ion-logo-instagram"
            link="https://to.pinjam.co/instagram"
          />
          <LinkIcon
            iconName="icon ion-logo-youtube"
            link="https://www.youtube.com/channel/UCq1UNnRIEiRVbPhiyCiDKfg"
          />
        </div>
      </div>
    );
  }
}

// export default withStyles(styles)(withRouter(Footer));
export default withStyles(styles)(withRouter(Footer));
