import React from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { withStyles } from "@material-ui/core/styles";

import { Typography, Slide, withWidth, Dialog } from "@material-ui/core/";

import isEmpty from "../../../utils/isEmpty";

import { clearNotification } from "../../../store/actions/notificationAction";
import NavBar from "../../mini/NavBar";
import { mainBgColor, black } from "../../../utils/ColorPicker";
import Loading from "../../mini/Loading";
import { getBusiness, getBusinessIDbySubdomain } from "../../../store/actions/businessAction";
import PageHelmet from "../../mini/PageHelmet";

import { nvhDefault, nvhMobile } from "../../../utils/navbarHeight";
import { createSession } from "../../../store/actions/customerAction";
import { getAllCatalog, setCartItem } from "../../../store/actions/cartAction";
import Logo from "../../../assets/logo.png";

const styles = theme => ({
	root: {
		flexGrow: 1,
		zIndex: 1,
		//overflow: "hidden",
		position: "relative",
		display: "flex",
		flexDirection: "column",
		width: "100%",
		backgroundColor: mainBgColor,
	},

	snackbarContainer: {
		width: "100%",
		height: "35px",
		position: "fixed",
		bottom: 0,
		zIndex: 1000,
	},
	errorSnackbar: {
		height: "100%",
		backgroundColor: "crimson",
		width: "100%",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
	},
	successSnackbar: {
		height: "100%",
		backgroundColor: "#00A86B",
		width: "100%",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
	},
	snackbarMsg: {
		color: "white",
		fontSize: "1.3rem",
	},
	content: {
		flexGrow: 1,
		display: "flex",
		flexDirection: "column",
		width: "100%",
	},
	main: {
		display: "flex",
		flexDirection: "column",
		alignSelf: "center",
		minHeight: `calc(100vh - ${nvhDefault})`,
		width: "100%",
		maxWidth: "600px",
		[theme.breakpoints.down("xs")]: {
			minHeight: `calc(100vh - ${nvhMobile})`,
		},
	},
	loadingBox: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		alignSelf: "center",
		height: "100vh",
		paddingBottom: "5rem",
		boxSizing: "border-box",
	},
	loadingBoxIcon: {
		width: "150px",
		height: "150px",
		marginBottom: "2rem",
	},
	DialogRoot: {
		backgroundColor: "rgba(0,0,0,0)",
		boxShadow: "none",
	},
});

class PublicComponent extends React.Component {
	state = {
		mobileOpen: false,
		errorSnackbar: false,
		successSnackbar: false,
		message: "",
		cartStatus: false,
		isReady: false,
	};

	numberWheelScroll = e => {
		if (document.activeElement.type === "number") {
			document.activeElement.blur();
		}
	};

	componentDidMount() {
		document.addEventListener("wheel", this.numberWheelScroll);

		// let bID = await this.props.getIDbySubdomain(
		//   this.props.pageProps.bizID
		// );

		// if (!isEmpty(bID)) {
		//   this.props.getAllCatalog(bID);
		//   this.props.getBusiness(bID);
		// } else {
		this.appReady();
		// }
	}

	componentWillReceiveProps(nextProps) {
		if (!isEmpty(nextProps.noti.message) && this.props.noti.message === "") {
			if (nextProps.noti.status === "error" && this.props.noti.status === "") {
				this.setState({
					errorSnackbar: true,
				});
			} else if (nextProps.noti.status === "success" && this.props.noti.status === "") {
				this.setState({
					successSnackbar: true,
				});
			}
			this.setState({
				message: nextProps.noti.message,
			});
			this.closeSnackbar();
			this.clearNoti();
		}
	}

	componentDidUpdate(prevProps, prevState) {}

	componentWillUnmount() {
		document.removeEventListener("wheel", this.numberWheelScroll);
	}

	appReady = () => {
		this.setState({
			isReady: true,
		});
	};

	closeSnackbar = () => {
		setTimeout(() => {
			this.setState({
				errorSnackbar: false,
				successSnackbar: false,
			});
		}, 2500);
	};

	clearNoti = () => {
		setTimeout(() => {
			this.props.clearNotification();
		}, 3000);
	};

	handleDrawerToggle = () => {
		this.setState({ mobileOpen: !this.state.mobileOpen });
	};

	handleCart = state => {
		this.setState({
			cartStatus: state,
		});
	};

	render() {
		const { width, classes, noNavbar, theme, secondaryNavbar } = this.props;
		const { errorSnackbar, successSnackbar, message, cartStatus } = this.state;
		const { status } = this.props.noti;
		const { detail } = this.props.business;
		const isLoading = this.props.loading.status;
		// const hideCart = this.props.router.asPath.includes("checkout");
		const hideMenu = true;

		if (this.state.isReady) {
			return (
				<div className={classes.root}>
					<PageHelmet
						metadata={{
							title: `StoreUp Checkout`,
						}}
					>
						<link rel="shortcut icon" href="../../../assets/logo.png" />
					</PageHelmet>

					{/* {isLoading && (
            <Dialog
              open={isLoading}
              elevation={0}
              classes={{
                paper: classes.DialogRoot,
              }}
            >
              <Loading open={isLoading} />
            </Dialog>
          )} */}

					{!isEmpty(this.props.noti.message) && this.props.noti.show && (
						<div className={classes.snackbarContainer}>
							{status === "error" && (
								<Slide in={errorSnackbar} direction="up">
									<div className={classes.errorSnackbar}>
										<Typography className={classes.snackbarMsg}>{message}</Typography>
									</div>
								</Slide>
							)}
							{status === "success" && (
								<Slide in={successSnackbar} direction="up">
									<div className={classes.successSnackbar}>
										<Typography className={classes.snackbarMsg}>{message}</Typography>
									</div>
								</Slide>
							)}
						</div>
					)}

					<div className={classes.content}>
						{isLoading && (
							<div className={classes.loadingBox}>
								<img src={Logo} className={classes.loadingBoxIcon} />
								<Loading open={isLoading} />
							</div>
						)}
						<div className={classes.main} style={isLoading ? { display: "none" } : {}}>
							{this.props.children}
						</div>
					</div>
				</div>
			);
		} else {
			return <div />;
		}
	}
}

const mapStateToProps = state => ({
	item: state.item,
	business: state.business,
	customer: state.customer,
	cart: state.cart,
	noti: state.notification,
	loading: state.loading,
});

export default connect(mapStateToProps, {
	clearNotification,
	getAllCatalog,
	getBusiness,
	getBusinessIDbySubdomain,
	createSession,
	setCartItem,
})(withStyles(styles, { withTheme: true })(withRouter(withWidth()(PublicComponent))));
