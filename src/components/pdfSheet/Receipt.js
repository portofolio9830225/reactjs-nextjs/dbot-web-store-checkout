import React, { Component } from "react";
import { connect } from "react-redux";
import { Typography, withStyles } from "@material-ui/core";
import isEmpty from "../../utils/isEmpty";

import { grey, greylight, greywhite } from "../../utils/ColorPicker";
import DateConverter from "../../utils/DateConverter";
import TimeConverter from "../../utils/TimeConverter";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
  },
  businessCard: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    margin: "1.5rem 0",
    width: "100%",
  },

  businessLogo: {
    width: "80px",
    height: "80px",
    borderRadius: "40px",
  },
  businessDetails: {
    display: "flex",
    flexDirection: "column",
    marginLeft: "1.5rem",
    [theme.breakpoints.down("xs")]: {
      marginLeft: "1rem",
    },
  },
  businessName: {
    fontSize: "1.5rem",
    fontWeight: 700,
    marginBottom: "0.5rem",
  },
  businessRegName: {
    fontSize: "1rem",
  },
  businessEmail: {
    fontSize: "1rem",
    fontWeight: 300,
  },
  businessPhone: {
    fontSize: "1rem",
    fontWeight: 300,
  },
  orderContent: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    boxSizing: "border-box",
    padding: "2rem 1.5rem",
    boxShadow:
      "-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
    // boxShadow: "5px 5px 8px #e3eeff, -5px -5px 5px #ffffff",
    borderRadius: "10px",
    width: "100%",
    marginTop: "1.5rem",
  },
  orderHeader: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: "40px",
    borderBottom: `1px solid ${greywhite}`,
    paddingBottom: "0.8rem",
  },
  sectionTitle: {
    fontSize: "1.1rem",
    fontWeight: 700,
    textTransform: "uppercase",
  },
  sectionBox: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    borderBottom: `1px solid ${greywhite}`,
    padding: "1rem 0",
  },
  itemBox: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "1.5rem 0",
    width: "100%",
  },
  itemBoxHeader: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  itemBoxBreakdown: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    marginTop: "1.5rem",
  },

  itemImg: {
    width: "60px",
    height: "60px",
  },
  itemName: {
    fontSize: "1.2rem",
    fontWeight: 500,
    paddingLeft: "1rem",
  },
  priceList: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginBottom: "0.3rem",
  },
  priceListTitle: {
    width: "53%",
    fontSize: "1rem",
    fontWeight: 300,
  },
  priceListText: {
    width: "45%",
    fontSize: "1rem",
    textAlign: "right",
  },
  totalBox: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    marginTop: "1rem",
    paddingTop: "1.5rem",
  },
});

class ReceiptSheet extends Component {
  render() {
    const { classes, toPrint } = this.props;
    const { bookDetails, bookItems, bookPayment } = this.props.booking;

    const VerticalDivider = (props) => {
      return (
        <div style={{ height: "100%", borderLeft: "1px solid gainsboro" }} />
      );
    };

    const TitleDesc = ({ title, desc }) => {
      return (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
          }}
        >
          <Typography style={{ fontSize: "0.8rem" }}>{title}</Typography>
          <Typography style={{ fontWeight: 700, fontSize: "0.9rem" }}>
            {desc}
          </Typography>
        </div>
      );
    };

    const ItemBox = ({ name, img, count, price, total, isFirst }) => {
      return (
        <div
          className={classes.itemBox}
          style={!isFirst ? { borderTop: `1px solid ${greywhite}` } : {}}
        >
          <div className={classes.itemBoxHeader}>
            <img
              // crossOrigin="anonymous"
              className={classes.itemImg}
              src={img}
            />
            <Typography className={classes.itemName}>{name}</Typography>
          </div>
          <div className={classes.itemBoxBreakdown}>
            <PriceList title="Product price" text={`RM${price.toFixed(2)}`} />
            <PriceList title="Quantity" text={`x ${count}`} />
            <PriceList end title="Subtotal" text={`RM${total.toFixed(2)}`} />
          </div>
        </div>
      );
    };

    const PriceList = ({ title, text, end }) => {
      return (
        <div className={classes.priceList}>
          <Typography className={classes.priceListTitle}>{title}</Typography>
          {/* <Typography style={{ fontSize: "0.9rem" }}>:</Typography> */}
          <Typography
            className={classes.priceListText}
            style={{ fontWeight: end ? 700 : 400 }}
          >
            {text}
          </Typography>
        </div>
      );
    };

    const IconText = ({ icon, text }) => {
      return (
        <div
          style={{ display: "flex", flexDirection: "row", margin: "0.3rem 0" }}
        >
          <i
            class={icon}
            style={{
              marginRight: "1rem",
              marginTop: "0.15rem",
              color: greylight,
            }}
          />
          <Typography
            style={{
              color: "black",
              fontSize: "0.9rem",
              fontWeight: 400,
            }}
          >
            {text}
          </Typography>
        </div>
      );
    };

    const DateTimeGetter = ({ dt }) => {
      let date = new Date(dt);

      let h = date.getHours();
      let min = date.getMinutes();

      let d = date.getDate();
      let month = date.getMonth() + 1;
      let y = date.getFullYear();

      return `${DateConverter(`${y}-${month}-${d}`)}, ${TimeConverter(
        `${h}:${min}:00`
      )}`;
    };

    return (
      <div className={classes.root}>
        <div className={classes.businessCard}>
          <img
            // crossOrigin="anonymous"
            className={classes.businessLogo}
            src={
              !isEmpty(bookDetails.business_logo)
                ? bookDetails.business_logo
                : "/images/noLogo.png"
            }
          />

          <div className={classes.businessDetails}>
            <Typography className={classes.businessName}>
              {bookDetails.business_name}
            </Typography>
            <Typography className={classes.businessRegName}>
              {bookDetails.reg_name}
            </Typography>
            {!isEmpty(bookDetails.business_email) && (
              <Typography className={classes.businessEmail}>
                {bookDetails.business_email}
              </Typography>
            )}
            {!isEmpty(bookDetails.business_phone) && (
              <Typography className={classes.businessPhone}>
                +60{bookDetails.business_phone}
              </Typography>
            )}
          </div>
        </div>
        {!isEmpty(bookDetails) && (
          <div
            className={classes.orderContent}
            style={
              toPrint
                ? { boxShadow: "none", border: `0.5px solid ${greywhite}` }
                : {}
            }
          >
            <div className={classes.orderHeader}>
              <div style={{ width: "47%" }}>
                <TitleDesc title="Order ID" desc={`${bookDetails.order_id}`} />
              </div>

              <VerticalDivider />
              <div style={{ width: "47%" }}>
                <TitleDesc
                  title="Receipt date"
                  desc={
                    <DateTimeGetter
                      dt={
                        process.env.NODE_ENV === "production"
                          ? bookPayment.paid_ts
                          : bookDetails.created_ts
                      }
                    />
                  }
                />
              </div>
            </div>
            <div className={classes.sectionBox}>
              <Typography
                className={classes.sectionTitle}
                style={{ marginBottom: "1rem" }}
              >
                Billed to
              </Typography>
              <IconText icon="fas fa-user" text={bookDetails.customer_name} />
              <IconText
                icon="fas fa-phone-alt"
                text={`+60${bookDetails.customer_phone}`}
              />
              {!isEmpty(bookDetails.address) && (
                <IconText icon="fas fa-envelope" text={bookDetails.address} />
              )}
            </div>
            <div className={classes.sectionBox}>
              <Typography
                className={classes.sectionTitle}
                style={{ marginBottom: "1rem" }}
              >
                Products
              </Typography>

              {bookItems.map((e, i) => {
                return (
                  <div>
                    <PriceList
                      key={i}
                      title={`${e.quantity}x ${e.item_name}`}
                      text={`RM${e.subtotal.toFixed(2)}`}
                    />
                  </div>
                );
              })}
            </div>

            <div className={classes.totalBox}>
              <PriceList
                title="Subtotal"
                text={`RM${bookDetails.items_subtotal.toFixed(2)}`}
              />
              <PriceList
                title="Shipping fee"
                text={`RM${bookDetails.shipping_price.toFixed(2)}`}
              />
              <div className={classes.priceList}>
                <Typography
                  className={classes.priceListTitle}
                  style={{ fontWeight: 700, fontSize: "1.5rem" }}
                >
                  TOTAL
                </Typography>
                {/* <Typography style={{ fontSize: "0.9rem" }}>:</Typography> */}
                <Typography
                  className={classes.priceListText}
                  style={{ fontWeight: 700, fontSize: "1.5rem" }}
                >
                  RM
                  {(
                    bookDetails.items_subtotal + bookDetails.shipping_price
                  ).toFixed(2)}
                </Typography>
              </div>
            </div>
          </div>
        )}
        <Typography
          style={{
            textAlign: "center",
            marginTop: "1.5rem",
            color: grey,
            width: "100%",
            maxWidth: "250px",
          }}
        >
          This is a computer generated receipt. No signature is required.
        </Typography>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  booking: state.booking,
});

export default connect(mapStateToProps)(withStyles(styles)(ReceiptSheet));
