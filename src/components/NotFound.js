import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Button, Typography } from "@material-ui/core";
import NotFoundLogo from "../assets/pageNotFound.svg";
import { greydark } from "../utils/ColorPicker";

const styles = (theme) => ({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
  },
  content: {
    height: "50vh",
    minHeight: "600px",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    height: "100%",
    width: "80%",
    maxHeight: "200px",
    marginBottom: "2rem",
    [theme.breakpoints.down("sm")]: {
      marginBottom: "1rem",
      maxHeight: "150px",
    },
  },
  title: {
    fontWeight: 500,
    width: "80%",
    maxWidth: "600px",
    fontSize: "2rem",
    textAlign: "center",
    color: greydark,
  },
  message: {
    width: "80%",
    maxWidth: "600px",
    marginBottom: "2rem",
    fontSize: "1.3rem",
    textAlign: "center",
    color: greydark,
    [theme.breakpoints.down("sm")]: {
      marginBottom: "1rem",
    },
  },
});

class NotFound extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.content}>
          <img src={NotFoundLogo} className={classes.logo} />
          <Typography className={classes.title}>Page not found</Typography>
          <Typography className={classes.message}>
            The link you followed may be broken.
          </Typography>
          <Button
            color="secondary"
            variant="contained"
            onClick={() => {
              window.location.replace("https://storeup.io");
            }}
          >
            Go back to StoreUp
          </Button>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(NotFound);
