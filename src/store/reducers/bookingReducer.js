import isEmpty from "../../utils/isEmpty";
import { GET_BOOK_DETAILS, GET_BANK, FPX_BANK, GET_GUEST, BILL_LINK, GET_BOOK_RECEIPT } from "../actions";

const initialState = {
	message: "",
	booking_id: "",
	priceDetails: {},
	bank: [],
	guest: {
		token: "",
		details: {},
	},
	bookDetails: {},
	bookItems: [],
	isBookingFetched: false,
	bookReceipt: {},
	bookPayment: {},
	bookPromo: {},
	bookShipping: {},
	fpx: [],
	billURL: "",
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_BANK:
			return {
				...state,
				bank: action.payload,
			};
		case GET_GUEST:
			return {
				...state,
				guest: {
					...state.guest,
					token: action.payload.token,
					details: action.payload.details,
				},
			};
		case GET_BOOK_DETAILS:
			let subtotal = 0;
			if (!isEmpty(action.payload)) {
				action.payload.products.map(p => {
					subtotal += p.price * p.quantity;
				});
			}

			let { products, payment, shipping, promo, ...detail } = action.payload;
			return {
				...state,
				bookDetails: !isEmpty(action.payload)
					? {
							...detail,
							// items_subtotal: subtotal,
							//addressData: action.payload.address_raw,
					  }
					: {},
				bookItems: !isEmpty(action.payload) ? products : [],
				bookPayment: payment,
				bookShipping: shipping,
				isBookingFetched: action.isFetch,
				bookPromo: promo,
			};
		case GET_BOOK_RECEIPT:
			return {
				...state,
				bookReceipt: action.payload.details,
			};
		case FPX_BANK:
			return {
				...state,
				fpx: action.payload,
			};
		case BILL_LINK:
			return {
				...state,
				billURL: action.payload,
			};
		default:
			return state;
	}
}
