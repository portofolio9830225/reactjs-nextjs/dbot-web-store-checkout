import isEmpty from "../../utils/isEmpty";
import { SET_CUSTOMER } from "../actions";

const initialState = {
  sessionID: null,
  name: null,
  phone: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_CUSTOMER:
      if (isEmpty(action.payload)) {
        return {
          ...state,
          sessionID: null,
          name: null,
          phone: null,
        };
      } else {
        return {
          ...state,
          sessionID: action.payload.session_id,
          name: action.payload.customer_name,
          phone: action.payload.customer_phone,
        };
      }

    default:
      return state;
  }
}
