import axios from "axios";

import {
	GET_BOOK_DETAILS,
	GET_BOOK_RECEIPT,
	GET_BANK,
	GET_GUEST,
	BILL_LINK,
	GET_BOOKING_ID,
	FPX_BANK,
	GET_WHATSAPP_DETAILS,
} from "./index";
import { setLoading } from "./loadingAction";
import { APILINK, XANO_APILINK } from "../../utils/key";
import isEmpty from "../../utils/isEmpty";
import { clearNotification, customNotification } from "./notificationAction";
import { getError, setInputError } from "./errorAction";
import { getBusinessIDbySubdomain } from "./businessAction";
import { endSession } from "./customerAction";
import isJson from "../../utils/isJson";

// export const createOrder = data => dispatch => {
// 	dispatch(setInputError());
// 	dispatch(clearNotification());
// 	dispatch(setLoading(true));
// 	axios
// 		.post(`${APILINK}/v1/order?session_id=${sessionStorage.getItem("sessionID")}`, data)
// 		.then(async res => {
// 			dispatch(clearNotification());
// 			dispatch(endSession());
// 			if (!isEmpty(res.data.payment_link)) {
// 				window.location.href = `${res.data.payment_link}?auto_submit=true`;
// 			} else {
// 				let host = window.location.hostname;
// 				let isProd = host.includes("storeup.site");
// 				let bizID = isProd ? host.split(".storeup.site")[0] : "inonity";
// 				window.location.href = `https://${bizID}.storeup.site/o/${res.data.order_id}`;
// 			}
// 			// dispatch({
// 			//   type: GET_BOOKING_ID,
// 			//   payload: res.data.payment_link,
// 			// });
// 		})
// 		.catch(err => {
// 			dispatch(setLoading(false));
// 			dispatch(getError(err));
// 		});
// };

export const completeOrder = (data, detail, shipping, bizID, shipType) => dispatch => {
	dispatch(setInputError());
	dispatch(clearNotification());
	dispatch(setLoading(true));
	axios
		.patch(`${XANO_APILINK}/order/${detail.ref}/complete`, data)
		.then(async res => {
			let localData = localStorage.getItem("@storeup:checkout");
			let custInfo = {};
			if (shipType !== 1 || !isJson(localData)) {
				custInfo = {
					name: detail.customer.name,
					phone: detail.customer.phone,
					email: detail.customer.email,
					address1: shipping ? shipping.address_1 : "",
					address2: shipping ? shipping.address_2 : "",
					address3: shipping ? shipping.address_3 : "",
					address4: shipping ? shipping.address_4 : "",
					postcode: shipping ? shipping.postcode : "",
					city: shipping ? shipping.city : "",
					state: shipping ? shipping.state : "",
					notes: data.notes,
				};
			} else {
				let parsed = JSON.parse(localData);
				custInfo = {
					...parsed,
					name: detail.customer.name,
					phone: detail.customer.phone,
				};
			}
			localStorage.setItem("@storeup:checkout", JSON.stringify(custInfo));

			dispatch(clearNotification());
			if (!isEmpty(res.data.payment_link)) {
				window.location.href = `${res.data.payment_link}?auto_submit=true`;
			} else {
				window.location.href = `https://${bizID}.storeup.site/o/${res.data.order.ref}`;
			}
		})
		.catch(err => {
			if (!err.response) {
				dispatch(customNotification("Network not detected", "error"));
			} else {
				dispatch(customNotification(err.response.data.message, "error", "expandBank"));
			}

			dispatch(setLoading(false));
			dispatch(getError(err), true);
		});
};

export const createLead = (data, sID, bID) => dispatch => {
	dispatch(setInputError());
	dispatch(clearNotification());
	dispatch(setLoading(true));

	//console.log(data);
	axios
		.post(`${XANO_APILINK}/order?session_id=${sID}`, data)
		.then(async res => {
			dispatch(setLoading(false));
			dispatch(clearNotification());
			dispatch(getBookDetails(res.data.order.ref, bID));

			dispatch({
				type: GET_WHATSAPP_DETAILS,
				payload: res.data.order_id,
				phone: res.data.contact_number,
			});
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getPaymentList = () => dispatch => {
	axios
		.get(`${XANO_APILINK}/bank/fpx`)
		.then(res => {
			dispatch({
				type: FPX_BANK,
				payload: res.data.banks,
			});
		})
		.catch(err => {
			dispatch(getError(err));
		});
};

export const getOrderBySessionID = sID => async dispatch => {
	let result = "";

	await axios
		.get(`${XANO_APILINK}/session/${sID}/order`)
		.then(res => {
			result = res.data.order.ref;
		})
		.catch(err => {
			dispatch(getError(err));
		});

	return result;
};

export const setItemOfOrder = (data, oID, bID) => dispatch => {
	dispatch(clearNotification());
	axios
		.patch(`${XANO_APILINK}/order/${oID}/product`, data)
		.then(async res => {
			dispatch(customNotification("", "success", "setItemOfOrder", false));
			dispatch(getBookDetails(oID, bID));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const editOrderCustomer = (data, oID, bID) => dispatch => {
	dispatch(clearNotification());
	axios
		.patch(`${XANO_APILINK}/order/${oID}/customer`, data)
		.then(async res => {
			dispatch(customNotification(res.data.message, "success", "editOrderCustomer", false));
			dispatch(getBookDetails(oID, bID));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const editOrderAddress = (data, oID, bID) => dispatch => {
	dispatch(clearNotification());
	axios
		.patch(`${XANO_APILINK}/order/${oID}/shipping`, data)
		.then(async res => {
			dispatch(customNotification(res.data.message, "success", "editOrderAddress", false));
			dispatch(getBookDetails(oID, bID));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const editOrderNotes = (data, oID, bID) => dispatch => {
	dispatch(clearNotification());
	axios
		.patch(`${XANO_APILINK}/order/${oID}/notes`, data)
		.then(async res => {
			dispatch(customNotification(res.data.message, "success", "editOrderNotes", false));
			dispatch(getBookDetails(oID, bID));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBookDetails = (id, bID) => dispatch => {
	if (id === null) {
		dispatch({
			type: GET_BOOK_DETAILS,
			payload: {},
		});
	} else {
		dispatch(setLoading(true));
		axios
			.get(`${XANO_APILINK}/order/${id}?business_id=${bID}`)
			.then(res => {
				dispatch(setLoading(false));

				dispatch({
					type: GET_BOOK_DETAILS,
					payload: {},
				});
				dispatch({
					type: GET_BOOK_DETAILS,
					payload: res.data.order,
					isFetch: true,
				});
			})
			.catch(err => {
				dispatch(setLoading(false));
				dispatch({
					type: GET_BOOK_DETAILS,
					payload: {},
					isFetch: true,
				});
				dispatch(getError(err));
			});
	}
};
