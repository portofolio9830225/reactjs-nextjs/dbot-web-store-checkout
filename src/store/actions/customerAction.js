import axios from "axios";
import isEmpty from "../../utils/isEmpty";
import { APILINK } from "../../utils/key";
import { SET_CUSTOMER } from "./";
import { getError, setInputError } from "./errorAction";
import { clearNotification, customNotification } from "./notificationAction";

export const createSession = (sID) => (dispatch) => {
  if (!isEmpty(eval(sID))) {
    dispatch(getSessionInfo(sID));
  } else {
    axios
      .post(`${APILINK}/v1/analytics/session`)
      .then((res) => {
        sessionStorage.setItem("sessionID", res.data.session_id);
        dispatch(getSessionInfo(res.data.session_id, true));
      })
      .catch((err) => {
        dispatch(getError(err));
      });
  }
};

export const updateSessionInfo = (sID, data) => (dispatch) => {
  dispatch(setInputError());
  dispatch(clearNotification());
  axios
    .patch(`${APILINK}/v1/analytics/session?session_id=${sID}`, data)
    .then((res) => {
      dispatch(
        customNotification(
          "Customer Info validated",
          "success",
          "sessionInfo",
          false
        )
      );
      dispatch(getSessionInfo(sID));
    })
    .catch((err) => {
      dispatch(getError(err));
    });
};

export const getSessionInfo = (sID, isInitial) => (dispatch) => {
  axios
    .get(`${APILINK}/v1/analytics/session?session_id=${sID}`)
    .then((res) => {
      if (isInitial) {
        dispatch({
          type: SET_CUSTOMER,
          payload: {},
        });
      }

      dispatch({
        type: SET_CUSTOMER,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(getError(err));
    });
};

export const endSession = () => (dispatch) => {
  sessionStorage.removeItem("sessionID");
  sessionStorage.removeItem("cart");
  dispatch({
    type: SET_CUSTOMER,
    payload: {},
  });
};
