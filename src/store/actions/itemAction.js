import axios from "axios";
import isEmpty from "../../utils/isEmpty";
import { APILINK } from "../../utils/key";
import { setLoading } from "./loadingAction";
import { getError } from "./errorAction";

import {
	GET_ITEM,
	CLEAR_ITEM_STATE,
	GET_ITEM_NA,
	GET_CART_ITEMS,
	DELETE_CART,
	CLEAR_CART,
	SET_CATEGORY,
} from "./index";
import { clearNotification, customNotification } from "./notificationAction";

export const clearItem = () => dispatch => {
	dispatch({
		type: CLEAR_ITEM_STATE,
	});
};

export const getItem = (itmid, bID) => dispatch => {
	dispatch(setLoading(true));
	axios
		.get(`${APILINK}/v1/item/${itmid}/public?business_id=${bID}&session_id=${sessionStorage.getItem("sessionID")}`)
		.then(res => {
			dispatch({
				type: GET_ITEM,
				payload: res.data,
			});
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch({
				type: GET_ITEM,
				payload: {},
			});
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};
