import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import isEmpty from "../utils/isEmpty";
import isJson from "../utils/isJson";
import { withStyles } from "@material-ui/core/styles";
import libphonenumber from "google-libphonenumber";

import { customNotification, clearNotification } from "../store/actions/notificationAction";
import { setInputError } from "../store/actions/errorAction";
import {
	createLead,
	getPaymentList,
	getBookDetails,
	getOrderBySessionID,
	setItemOfOrder,
	editOrderAddress,
	editOrderNotes,
	completeOrder,
	editOrderCustomer,
} from "../store/actions/bookingAction";
import { getBusiness } from "../store/actions/businessAction";
import { setLoading } from "../store/actions/loadingAction";
import {
	getShippingCatalog,
	setCartItem,
	// getShippingItem,
} from "../store/actions/cartAction";

import {
	withMobileDialog,
	withWidth,
	Typography,
	InputAdornment,
	Fade,
	Slide,
	Collapse,
	Button,
} from "@material-ui/core";
import { black, grey, greydark, mainBgColor, secondary } from "../utils/ColorPicker";
import CheckoutLogo from "../assets/StoreUpCheckout_black.svg";
import ExpandMoreIcon from "../assets/ExpandMore.svg";

import BookingAddress from "../components/BookingAddress";
import BookingBank from "../components/BookingBank";
import PriceList from "../components/mini/PriceList";
import TextInput from "../components/mini/TextInput";
import NotFound from "../components/NotFound";
import { ChevronLeftRounded, ChevronRightRounded, ExpandLessRounded, ExpandMoreRounded } from "@material-ui/icons";
import isEmail from "../utils/isEmail";

const displayBoxContentMainCollapse = "calc(100% - 20px)";

const styles = theme => ({
	root: {
		flex: 1,
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "100%",
		position: "relative",
		paddingTop: "2rem",
	},
	main: {
		marginTop: "1rem",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		width: "100%",
		height: "100%",
		[theme.breakpoints.down("xs")]: {
			width: "90%",
		},
	},
	title: {
		fontSize: "2rem",
		letterSpacing: "0.5px",
		fontWeight: 700,
		width: "100%",
	},
	desc: {
		fontSize: "1.2rem",
		fontWeight: 300,
		color: greydark,
		width: "100%",
		marginBottom: "2rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
			marginBottom: "1.5rem",
		},
	},
	ProductList: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginBottom: "1rem",
	},
	ProductListImage: {
		width: "15%",
		position: "relative",
		// borderRadius: "1rem",
		// overflow: "hidden",
	},
	ProductListTitle: {
		width: "53%",
		fontSize: "1rem",
		fontWeight: 400,
	},
	ProductListText: {
		width: "25%",
		fontSize: "1rem",
		textAlign: "right",
	},
	ProductListTextAnother: {
		fontSize: "0.9rem",
		textAlign: "right",
		textDecoration: "line-through",
	},
	businessCard: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		margin: "1.5rem 0",
	},
	logo: {
		width: "150px",
		height: "150px",
		borderRadius: "75px",
	},
	businessName: {
		marginTop: "1rem",
		fontSize: "1.7rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.3rem",
			textAlign: "center",
		},
	},
	textField: {
		width: "100%",
		marginBottom: "1.5rem",
	},
	sectionTitle: {
		color: grey,
		textTransform: "uppercase",
		fontSize: "1.1rem",
		marginBottom: "1rem",
		fontWeight: 400,
		width: "17%",
		// marginTop: "3rem",
		marginBottom: "0.5rem",
		[theme.breakpoints.down("xs")]: {
			width: "23%",
			fontSize: "1.1rem",
		},
	},
	listingSection: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		//marginBottom: "3rem",
	},
	emptyList: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		margin: "4rem 0",
		width: "100%",
	},
	emptyListText: {
		fontWeight: 500,
		color: greydark,
		marginTop: "1rem",
		fontSize: "1.2rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
			marginTop: "0.5rem",
		},
	},
	headerTitle: {
		alignSelf: "flex-start",
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		marginBottom: "0.5rem",
	},
	headerTitleText: {
		marginLeft: "0.8rem",
		color: greydark,
		textTransform: "uppercase",
		fontSize: "1.3rem",
		fontWeight: 500,
		width: "100%",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
			marginBottom: 0,
		},
	},
	titleDesc: {
		display: "flex",
		flexDirection: "column",
		//marginBottom: "1rem",
	},
	titleDescTitle: {
		fontSize: "0.9rem",
		fontWeight: 300,
		color: "grey",
	},
	titleDescDesc: {
		fontSize: "1.1rem",
	},
	displayBox: {
		//boxShadow: "3px 3px 15px #d6d6e3, -4px -4px 20px #ffffff",
		borderBottom: "1px solid gainsboro",
		//background: "linear-gradient(309.34deg, #F2F3F6 -13.68%, #E5E6EC 171.92%)",
		//borderRadius: "10px",
		boxSizing: "border-box",
		padding: "1.5rem 0",
		//marginBottom: "2rem",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		[theme.breakpoints.down("xs")]: {
			padding: "0.8rem 0",
		},
	},
	displayBoxContent: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",

		width: "78%",
		boxSizing: "border-box",
		[theme.breakpoints.down("xs")]: {
			paddingLeft: "1rem",
			width: "72%",
		},
	},
	displayBoxContentMain: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
	},
	nextButton: {
		alignSelf: "flex-end",
		//marginTop: "1rem",
		fontSize: "0.9rem",
		padding: "0.5rem 2rem",
		[theme.breakpoints.down("xs")]: {
			padding: "0.5rem 1.5rem",
		},
	},
	backIconContainer: {
		marginBottom: "1.5rem",
		alignSelf: "flex-start",
		transition: "all 0.3s",
		cursor: "pointer",
		boxShadow:
			"-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		height: "40px",
		width: "40px",
		borderRadius: "50%",
		// background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		// "&:active": {
		//   background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		// },
	},
	backIcon: {
		color: greydark,
		fontSize: "1.8rem",
	},
	editButtonContainer: {
		//cursor: "pointer",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
	},
	editButton: {
		color: secondary,
		fontSize: "1.9rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.3rem",
		},
		// paddingRight: "0.8rem",
		// paddingBottom: "0.5rem",
		// textDecoration: "underline",
		//alignSelf: "center",
	},
	DialogRoot: {
		backgroundColor: "rgba(0,0,0,0)",
		boxShadow: "none",
		display: "flex",
		borderRadius: 0,
	},
	DialogRootItem: {
		backgroundColor: mainBgColor,
		boxShadow: "none",
		display: "flex",
		height: "100%",
		borderRadius: 0,
		position: "relative",
	},
	DialogPaperScrollPaper: {
		maxHeight: "100%",
		display: "flex",
		alignItems: "center",
	},
	BankMenuItem: {
		paddingTop: "1.5rem",
		paddingBottom: "1.5rem",
	},
});

class Checkout extends Component {
	state = {
		localData: {},
		notFound: false,
		isReady: false,
		isWhatsapp: false,
		isOrderFetched: false,
		isCustomer: true,
		isAddress: false,
		isNotes: false,
		isPayment: false,
		bID: "",
		name: "",
		phone: "",
		email: "",
		concern: "",
		step: 1,
		cartHeight: 0,
		shipType: null,
		pMethod: "",
		bank: "",
		note: "",
		sZone: "",
		address1: "",
		address2: "",
		address3: "",
		address4: "",
		postcode: "",
		ciName: "",
		staName: "",
		country: "Malaysia",
		plShippingName: "",
		plShippingPrice: "",
		isLead: false,
		requestedItem: [],
		breakdownTotal: 0,
	};

	appReady = () => {
		this.setState({
			isReady: true,
		});
	};

	updateInputFromLocalData = () => {
		if (!isEmpty(this.state.localData)) {
			let { localData } = this.state;
			this.setState({
				name: localData.name || "",
				phone: localData.phone || "",
				email: localData.email || "",
				address1: localData.address1 || "",
				address2: localData.address2 || "",
				address3: localData.address3 || "",
				address4: localData.address4 || "",
				postcode: localData.postcode || "",
				ciName: localData.city || "",
				staName: localData.state || "",
				note: localData.notes || "",
			});
		}
	};

	async componentDidMount() {
		this.props.setLoading(true);
		let params = new URLSearchParams(window.location.search);
		let bID = params.get("bid");
		let data = params.get("data");
		let oID = params.get("oid");
		let waContact = params.get("wa");
		let localData = localStorage.getItem("@storeup:checkout");

		this.setState(
			{
				notFound: isEmpty(bID) || (!isJson(data) && isEmpty(oID)),
				localData: isJson(localData) ? JSON.parse(localData) : {},
				bID,
			},
			async () => {
				if (!this.state.notFound) {
					this.updateInputFromLocalData();
					console.log(bID);
					this.props.getBusiness(bID);
					if (!isEmpty(oID)) {
						this.props.getBookDetails(oID, bID);
					} else if (isJson(data)) {
						let d = JSON.parse(data);
						if (isEmpty(d.session_id)) {
							this.appReady();
						} else {
							let orderID = await this.props.getOrderBySessionID(d.session_id);

							if (!isEmpty(orderID)) {
								let products = [];
								d.items.map(p => {
									products.push({
										product_ref: p.item_id,
										quantity: p.quantity,
										variant_id: p.variant_number,
									});
								});
								let data = {
									products,
								};
								this.props.setItemOfOrder(data, orderID, bID);

								// this.props.getBookDetails(oID, bID);
							} else {
								this.setState(
									{
										data: d,
										isWhatsapp: waContact === "true",
									},
									() => {
										if (!this.state.isWhatsapp) {
											//this.props.getPaymentMethod(this.state.data.catalog_id);
											this.props.getPaymentList();
											//this.props.getShippingCatalog(this.state.data.catalog_id, bID);
										}
										this.props.setCartItem(this.state.data.items, bID, null, d.session_id);
									}
								);
							}
						}
					}
				} else {
					this.appReady();
				}
			}
		);
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			(isEmpty(prevProps.booking.bookItems) && !isEmpty(this.props.booking.bookItems)) ||
			(isEmpty(prevProps.business.listings) && !isEmpty(this.props.business.listings))
		) {
			this.setState(
				{
					isOrderFetched: !isEmpty(this.props.booking.bookItems),
				},
				() => {
					if (!this.state.isOrderFetched) {
						let reqItem = [];
						let total = 0;
						this.props.cart.list.map((e, i) => {
							let item = this.props.business.listings.find(f => e.item_id === f.ref);
							if (!isEmpty(item)) {
								total += item.price * e.quantity;

								reqItem.push({
									image_url: item.images[0].url_s,
									item_name: item.name,
									variant_name: !isEmpty(e.variant_name) ? `(${e.variant_name})` : "",
									quantity: e.quantity,
									item_price: item.price,
									another_price: item.another_price,
								});
							}
						});
						this.setState({
							breakdownTotal: total,
							requestedItem: reqItem,
						});
					}

					this.appReady();
				}
			);
		}

		if (!prevState.isOrderFetched && this.state.isOrderFetched) {
			let params = new URLSearchParams(window.location.search);
			if (isEmpty(params.get("oid"))) {
				params.set("oid", this.props.booking.bookDetails.ref);
				params.delete("data");
				params.delete("wa");
				window.history.replaceState(null, null, `?${params.toString()}`);
			}
			if (this.state.isWhatsapp) {
				this.handleWhatsapp();
			} else {
				let { bookDetails, bookPayment } = this.props.booking;
				let businessDetail = this.props.business.detail;

				let { addressData } = bookDetails;

				if (isEmpty(bookPayment)) {
					this.setState({
						isCustomer: false,
						name: bookDetails.customer.name,
						phone: bookDetails.customer.phone,
						email: bookDetails.customer.email,
						isNotes: true,
						isPayment: true,
						isAddress: false,
						isAddress: businessDetail.shipping_type == 2 || businessDetail.shipping_type == 1,
						// isAddress:
						//   shipType === 2 ||
						//   shipType === 4 ||
						//   (shipType === 3 &&
						//     !isEmpty(addressData) &&
						//     isEmpty(addressData.address_1)),
						address1: addressData ? addressData.address_1 : this.state.address1,
						address2: addressData ? addressData.address_2 : this.state.address2,
						address3: addressData ? addressData.address_3 : this.state.address3,
						address4: addressData ? addressData.address_4 : this.state.address4,
						postcode: addressData ? addressData.postcode : this.state.postcode,
						ciName: addressData ? addressData.city : this.state.ciName,
						staName: addressData ? addressData.state : this.state.staName,
					});

					this.props.getPaymentList();
				} else {
					window.location.replace(
						`https://${this.props.business.detail.subdomain}.storeup.site/o/${bookDetails.ref}`
					);
				}
			}
		}
		// if (
		//   isEmpty(prevProps.booking.bookDetails) &&
		//   !isEmpty(this.props.booking.bookDetails)
		// ) {
		//   let { addressData } = this.props.booking.bookDetails;
		//   let { shipType } = this.state;
		//   this.setState({
		//     isAddress:
		//       shipType === 2 ||
		//       shipType === 4 ||
		//       (shipType === 3 &&
		//         !isEmpty(addressData) &&
		//         isEmpty(addressData.address_1)),
		//     address1: addressData.address_1 || this.state.address1,
		//     address2: addressData.address_2 || this.state.address2,
		//     address3: addressData.address_3 || this.state.address3,
		//     address4: addressData.address_4 || this.state.address4,
		//     postcode: addressData.postcode || this.state.postcode,
		//     ciName: addressData.city || this.state.ciName,
		//     staName: addressData.state || this.state.staName,
		//   });
		// }
		if (isEmpty(prevProps.notification.redirect) && this.props.notification.redirect === "editOrderCustomer") {
			this.setState({
				isCustomer: false,
			});
		}
		if (isEmpty(prevProps.notification.redirect) && this.props.notification.redirect === "editOrderAddress") {
			this.setState({
				isAddress: false,
			});
		}
		if (isEmpty(prevProps.notification.redirect) && this.props.notification.redirect === "editOrderNotes") {
			this.setState({
				isNotes: false,
			});
		}
		if (isEmpty(prevProps.notification.redirect) && this.props.notification.redirect === "expandBank") {
			this.setState({
				isPayment: true,
			});
		}

		if (!prevState.isReady && this.state.isReady) {
			this.props.setLoading(false);
		}

		if (isEmpty(prevProps.cart.shipping) && !isEmpty(this.props.cart.shipping)) {
			let { is_zone_based, is_weight_based, is_product_based } = this.props.cart.shipping;
			let { bookDetails } = this.props.booking;
			let shipType = 1;
			if (is_zone_based === 1 && is_weight_based === 0 && is_product_based === 0) {
				shipType = 2;
			}
			if (is_zone_based === 0 && is_weight_based === 1 && is_product_based === 0) {
				shipType = 3;
			}
			if (is_zone_based === 0 && is_weight_based === 0 && is_product_based === 1) {
				shipType = 4;
			}

			this.setState(
				{
					shipType,
					isAddress: true,
				},
				() => {
					//console.log("shiptype", this.state.shipType, "isAddress", this.state.isAddress);
				}
			);
		}
	}

	setResizer = () => {
		let cartEl = document.getElementById("cartCard");
		this.resizeObserver.observe(cartEl);
	};

	resizeObserver = new ResizeObserver(entries =>
		this.setState({
			cartHeight: entries[0].target.clientHeight,
		})
	);

	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			this.props.router.push(link);
		}
	};

	handleSelect = name => event => {
		this.setState({ [name]: event.target.value });
	};

	handlePrebookChange = data => {
		this.setState({
			...data,
		});
	};

	handleText =
		(name, limit = 999) =>
		event => {
			let val = event.target.value;

			if (val.length <= limit) {
				this.setState({ [name]: val });
			}
		};

	handleCustomer = () => {
		this.props.setInputError();
		this.props.clearNotification();
		let err = [];
		if (isEmpty(this.state.name)) {
			err.push({ param: "customer_name", error: "Missing value" });
		}

		if (isEmpty(this.state.email)) {
			err.push({ param: "customer_email", error: "Missing value" });
		} else if (!isEmail(this.state.email)) {
			err.push({ param: "customer_email", error: "Invalid email" });
		}

		if (isEmpty(this.state.phone)) {
			err.push({ param: "customer_phone", error: "Missing value" });
		} else {
			const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
			const phoneNum = phoneUtil.parseAndKeepRawInput(this.state.phone, "MY");

			if (
				!phoneUtil.isValidNumberForRegion(phoneNum, "MY") ||
				phoneUtil.getNumberType(phoneNum) != libphonenumber.PhoneNumberType.MOBILE
			) {
				err.push({ param: "customer_phone", error: "Invalid phone number" });
			}
		}

		if (!isEmpty(err)) {
			this.props.customNotification("Invalid information", "error");
			this.props.setInputError(err);
		} else {
			if (!this.state.isOrderFetched) {
				let params = new URLSearchParams(window.location.search);
				let d = JSON.parse(params.get("data"));
				let products = [];
				d.items.map(p => {
					products.push({ product_ref: p.item_id, quantity: p.quantity, variant_id: p.variant_number });
				});
				let data = {
					name: this.state.name,
					phone: this.state.phone,
					email: this.state.email.toLowerCase(),
					business_id: params.get("bid"),
					//catalog_id: d.catalog_id,
					products,
				};
				this.props.createLead(data, d.session_id, params.get("bid"));
			} else {
				let data = {
					name: this.state.name,
					phone: this.state.phone,
					email: this.state.email.toLowerCase(),
				};

				this.props.editOrderCustomer(data, this.props.booking.bookDetails.ref, this.state.bID);
			}
		}
	};

	handleAddress = () => {
		let { detail } = this.props.business;
		let data = {
			address_1: this.state.address1,
			address_2: this.state.address2,
			address_3: this.state.address3,
			address_4: this.state.address4,
			postcode: this.state.postcode,
			city: this.state.ciName,
			state: this.state.staName,
			country: "MY",
			//zone_number: this.state.sZone,
			shipping_custom_id: detail.shipping_type == 1 ? this.state.sZone : undefined,
			service_id: detail.shipping_type == 2 ? this.state.sZone : undefined,
		};

		this.props.editOrderAddress(data, this.props.booking.bookDetails.ref, this.state.bID);
	};

	handleNotes = () => {
		let data = {
			notes: this.state.note,
		};

		this.props.editOrderNotes(data, this.props.booking.bookDetails.ref, this.state.bID);
	};

	handleWhatsapp = () => {
		let itemArr = "";
		this.props.booking.bookItems.map((e, i) => {
			itemArr += `*${i + 1}. ${e.name} (x${e.quantity})*\n`;
		});

		let text = `[  _Order ID:_ *${this.props.booking.bookDetails.ref}*  ]\nHi ${
			this.props.booking.bookDetails.business.name
		}, ${
			this.props.booking.bookDetails.customer.name
		} here.\n\nI'm interested in these products:-\n${itemArr}\nCan I know more about the products?\n\n\n_*Pssst...*_\n${"```"}You may proceed with your order using this link${"```"}\nhttps://checkout.storeup.io?oid=${
			this.props.booking.bookDetails.ref
		}&bid=${this.props.booking.bookDetails.business.id}\n\n${this.state.concern}`;

		this.props.setLoading(false);
		window.location.href = `https://wa.me/60${
			this.props.booking.bookDetails.business.phone
		}?text=${encodeURIComponent(text)}`;
	};

	handleSubmit = () => {
		if (this.state.isWhatsapp) {
			this.handleCustomer();
		} else {
			let data = {
				bank_code: this.state.bank,
				payment_method: this.state.pMethod,
			};
			this.props.completeOrder(
				data,
				this.props.booking.bookDetails,
				this.props.booking.bookShipping,
				this.props.business.detail.subdomain,
				this.state.shipType
			);
		}
	};

	handleBack = () => {
		window.location.replace(`https://${this.props.business.detail.subdomain}.storeup.site`);
	};

	render() {
		const { classes } = this.props;
		const {
			isReady,
			notFound,
			isWhatsapp,
			isOrderFetched,
			name,
			phone,
			email,
			concern,
			step,
			shipType,
			address1,
			address2,
			address3,
			address4,
			postcode,
			ciName,
			staName,
			country,
			plShippingName,
			plShippingZone,
			pMethod,
			sZone,
			note,
			bank,
			isCustomer,
			isAddress,
			isNotes,
			isPayment,
			requestedItem,
			breakdownTotal,
		} = this.state;
		const { detail, listings } = this.props.business;
		const { bookDetails, bookShipping, bookItems, fpx } = this.props.booking;
		const { list, breakdown, shipping } = this.props.cart;
		const is3PL = this.props.business.detail.shipping_type == 2;
		const isNeededAddress = is3PL || this.props.business.detail.shipping_type == 1;

		const isOneActive = isCustomer || (isAddress && isNeededAddress) || isNotes || isPayment;

		const StoreUpTag = () => {
			return (
				<div
					style={{
						display: "flex",
						marginBottom: "0.5rem",
						position: "absolute",
						bottom: 0,
						cursor: "pointer",
						padding: "0.5rem 1rem",
					}}
					onClick={this.handlePage("https://storeup.io")}
				>
					<Typography style={{ fontWeight: 300 }}>Powered by</Typography>
					<Typography style={{ fontWeight: 700, marginLeft: "0.3rem" }}>StoreUp</Typography>
				</div>
			);
		};

		const TitleDesc = props => (
			<div className={classes.titleDesc}>
				{/* <Typography className={classes.titleDescTitle}>
          {props.title}
        </Typography> */}
				<Typography className={classes.titleDescDesc}>{props.desc}</Typography>
			</div>
		);

		const ProductList = ({ title, desc, quantity, text, image, end, another_price }) => {
			return (
				<div className={classes.ProductList}>
					<div className={classes.ProductListImage}>
						<img src={image} style={{ width: "100%", borderRadius: "1rem" }} />
						{!isEmpty(another_price) && (
							<div
								style={{
									position: "absolute",
									width: "50px",
									backgroundColor: "#FF9500",
									padding: "0.2rem 0.06rem",
									bottom: "-3px",
									textAlign: "center",
									fontSize: "0.60rem",
									borderRadius: "12px",
									left: "19px",
									color: "white",
								}}
							>
								{parseInt(((text - another_price) / text) * 100)}% OFF
							</div>
						)}
					</div>
					<div className={classes.ProductListTitle}>
						<Typography>{title}</Typography>
						<Typography style={{ color: "dimgrey" }}>{desc}</Typography>
						<Typography>x{quantity}</Typography>
					</div>
					{/* <Typography style={{ fontSize: "0.9rem" }}>:</Typography> */}
					{isEmpty(another_price) ? (
						<Typography className={classes.ProductListText} style={{ fontWeight: end ? 700 : 400 }}>
							{text}
						</Typography>
					) : (
						<div>
							<Typography className={classes.ProductListText} style={{ fontWeight: end ? 700 : 400 }}>
								RM{another_price.toFixed(2)}
							</Typography>
							<Typography className={classes.ProductListTextAnother}>RM{text.toFixed(2)}</Typography>
						</div>
					)}
				</div>
			);
		};

		if (isReady) {
			if (!notFound) {
				return (
					<div
						className={classes.root}
						style={{
							marginBottom: this.state.cartHeight + 150,
						}}
					>
						<Fade in={step === 1}>
							<div className={classes.main}>
								{/* <div
                  className={classes.backIconContainer}
                  onClick={this.handleBack}
                >
                  <ChevronLeftRounded className={classes.backIcon} />
                </div> */}
								{isWhatsapp ? (
									<Typography className={classes.title}>Contact business</Typography>
								) : (
									<div
										style={{
											width: "35%",
											minWidth: "100px",
											alignSelf: "flex-start",
										}}
									>
										<img src={CheckoutLogo} style={{ width: "100%" }} />
									</div>
								)}

								<Typography className={classes.desc}>
									Complete the required information
									{/* {isWhatsapp
                    ? "Complete details and directly contact business thorugh Whatsapp"
                    : "Complete details to proceed with the payment"} */}
								</Typography>
								<Typography className={classes.sectionTitle} style={{ width: "100%" }}>
									Products
								</Typography>

								{isOrderFetched
									? bookItems.map((e, i) => {
											return (
												<ProductList
													key={i}
													image={e.image}
													title={`${e.name}`}
													quantity={e.quantity}
													desc={!isEmpty(e.variant_name) ? `(${e.variant_name})` : ""}
													text={e.price}
													another_price={e.another_price}
												/>
											);
									  })
									: requestedItem.map((e, i) => {
											return (
												<ProductList
													key={i}
													image={e.image_url}
													title={`${e.item_name}`}
													desc={!isEmpty(e.variant_name) ? `(${e.variant_name})` : ""}
													quantity={e.quantity}
													text={e.item_price}
													another_price={e.another_price}
													// another_price={`RM${e.another_price.toFixed(2)}`}
												/>
											);
									  })}
								{/* : listings
											.filter(
												e => !isEmpty(this.props.cart.list.find(f => e.item_id === f.item_id))
											)
											.map((e, i) => {
												return (
													<ProductList
														key={i}
														image={e.image_url}
														title={` ${e.item_name}`}
														quantity={
															this.props.cart.list.find(f => e.item_id === f.item_id)
																.quantity
														}
														text={`RM${e.item_price.toFixed(2)}`}
													/>
												);
											})} */}
								<div
									style={{
										marginTop: "1rem",
										borderTop: "1px solid gainsboro",
										width: "100%",
									}}
								/>
								<div className={classes.listingSection}>
									<div
										className={classes.displayBox}
										style={isWhatsapp ? { borderBottom: "none" } : {}}
									>
										<Typography className={classes.sectionTitle}>Customer</Typography>
										{/* {!isOneActive && (
                      <Fade in={!isOneActive}>
                        <Typography
                          className={classes.editButton}
                          onClick={() => {
                            this.setState({
                              isCustomer: true,
                            });
                          }}
                        >
                          Edit
                        </Typography>
                      </Fade>
                    )} */}

										{isCustomer && (
											<Fade in={isCustomer}>
												<div className={classes.displayBoxContent}>
													<div className={classes.displayBoxContentMain}>
														<TextInput
															color="secondary"
															className={classes.textField}
															variant="outlined"
															label="Full name"
															placeholder="Full name"
															value={name}
															onChange={this.handleText("name")}
															errorkey="customer_name"
														/>
														<TextInput
															color="secondary"
															className={classes.textField}
															variant="outlined"
															InputProps={{
																startAdornment: (
																	<InputAdornment position="start">
																		+60
																	</InputAdornment>
																),
															}}
															label="Whatsapp number"
															placeholder="Whatsapp number"
															type="number"
															value={phone}
															onChange={this.handleText("phone", 10)}
															errorkey="customer_phone"
														/>
														<TextInput
															color="secondary"
															className={classes.textField}
															variant="outlined"
															label="Email"
															placeholder="Email"
															value={email}
															onChange={this.handleText("email")}
															errorkey="customer_email"
														/>
														{isWhatsapp && (
															<TextInput
																color="secondary"
																multiline
																rows={3}
																className={classes.textField}
																variant="outlined"
																label="What is your concern? (Optional)"
																placeholder="Write your concern"
																value={concern}
																onChange={this.handleText("concern")}
															/>
														)}
														{!isWhatsapp && (
															<Button
																color="secondary"
																variant="contained"
																onClick={this.handleCustomer}
																className={classes.nextButton}
															>
																Save
															</Button>
														)}
													</div>
												</div>
											</Fade>
										)}
										{!isCustomer && (
											<Fade in={!isCustomer}>
												<div
													className={classes.displayBoxContent}
													style={{
														cursor: isOrderFetched ? "pointer" : "default",
													}}
													onClick={
														isOrderFetched && !isCustomer
															? () => {
																	this.setState({
																		isCustomer: true,
																	});
															  }
															: null
													}
												>
													<div
														className={classes.displayBoxContentMain}
														style={{ width: displayBoxContentMainCollapse }}
													>
														<TitleDesc title="Name" desc={name} />
														<TitleDesc title="Phone number" desc={`+60${phone}`} />
														<TitleDesc title="Email" desc={email} />
													</div>
													<div className={classes.editButtonContainer}>
														<img src={ExpandMoreIcon} style={{ width: "15px" }} />
													</div>
												</div>
											</Fade>
										)}
									</div>
									{!isWhatsapp && (
										// isOrderFetched &&
										<>
											{isNeededAddress && (
												<div className={classes.displayBox}>
													<Typography className={classes.sectionTitle}>Shipping</Typography>
													{isAddress && (
														<Fade in={isAddress}>
															<div className={classes.displayBoxContent}>
																<div className={classes.displayBoxContentMain}>
																	<BookingAddress
																		address1={address1}
																		address2={address2}
																		address3={address3}
																		address4={address4}
																		postcode={postcode}
																		ciName={ciName}
																		staName={staName}
																		sZone={sZone}
																		plShippingName={plShippingName}
																		plShippingZone={plShippingZone}
																		isProduct={shipType === 4}
																		isWeightBased={shipType === 3}
																		onChange={this.handlePrebookChange}
																		{...this.state}
																	/>
																	<Button
																		color="secondary"
																		variant="contained"
																		onClick={this.handleAddress}
																		className={classes.nextButton}
																	>
																		Save
																	</Button>
																</div>
															</div>
														</Fade>
													)}

													{!isAddress && (
														<Fade in={!isAddress}>
															<div
																className={classes.displayBoxContent}
																style={{
																	cursor: isOrderFetched ? "pointer" : "default",
																}}
																onClick={
																	isOrderFetched && !isAddress
																		? () => {
																				this.setState({
																					isAddress: true,
																				});
																		  }
																		: null
																}
															>
																<div
																	className={classes.displayBoxContentMain}
																	style={{
																		width: displayBoxContentMainCollapse,
																	}}
																>
																	{(shipType === 2 || shipType === 4 || is3PL) && (
																		<TitleDesc
																			title="Shipping rate"
																			desc={
																				!isEmpty(bookDetails) && !isEmpty(sZone)
																					? bookDetails.shipping_name
																					: "No rate selected"
																			}
																		/>
																	)}
																	<TitleDesc
																		title="Address"
																		desc={
																			!isEmpty(this.state.address1)
																				? `${this.state.address1} ${
																						this.state.address2 || ""
																				  }${this.state.address3 || ""}${
																						this.state.address4 || ""
																				  } ${this.state.postcode} ${
																						this.state.ciName
																				  }, ${this.state.staName}, ${
																						this.state.country
																				  }`
																				: "No address yet"
																		}
																	/>
																</div>
																<div className={classes.editButtonContainer}>
																	<img
																		src={ExpandMoreIcon}
																		style={{ width: "15px" }}
																	/>
																</div>
															</div>
														</Fade>
													)}
												</div>
											)}

											<div className={classes.displayBox}>
												<Typography className={classes.sectionTitle}>Notes</Typography>

												{isNotes && (
													<Fade in={isNotes}>
														<div className={classes.displayBoxContent}>
															<div className={classes.displayBoxContentMain}>
																<TextInput
																	color="secondary"
																	multiline
																	rows={3}
																	className={classes.textField}
																	variant="outlined"
																	label="Note to seller (optional)"
																	placeholder="Optional"
																	value={note}
																	onChange={this.handleText("note")}
																/>

																<Button
																	color="secondary"
																	variant="contained"
																	onClick={this.handleNotes}
																	className={classes.nextButton}
																>
																	Save
																</Button>
															</div>
														</div>
													</Fade>
												)}
												{!isNotes && (
													<Fade in={!isNotes}>
														<div
															className={classes.displayBoxContent}
															style={{
																cursor: isOrderFetched ? "pointer" : "default",
															}}
															onClick={
																isOrderFetched && !isNotes
																	? () => {
																			this.setState({
																				isNotes: true,
																			});
																	  }
																	: null
															}
														>
															<div
																className={classes.displayBoxContentMain}
																style={{ width: displayBoxContentMainCollapse }}
															>
																<TitleDesc
																	title="Note to seller"
																	desc={!isEmpty(note) ? note : "No additional notes"}
																/>
															</div>
															<div className={classes.editButtonContainer}>
																<img src={ExpandMoreIcon} style={{ width: "15px" }} />
															</div>
														</div>
													</Fade>
												)}
											</div>
											<div className={classes.displayBox}>
												<Typography className={classes.sectionTitle}>Payment</Typography>

												{isPayment && (
													<Fade in={isPayment}>
														<div className={classes.displayBoxContent}>
															<div className={classes.displayBoxContentMain}>
																<BookingBank
																	onChange={this.handlePrebookChange}
																	{...this.state}
																/>
																<Button
																	color="secondary"
																	variant="contained"
																	onClick={() => {
																		this.setState({
																			isPayment: false,
																		});
																	}}
																	className={classes.nextButton}
																>
																	Save
																</Button>
															</div>
														</div>
													</Fade>
												)}
												{!isPayment && (
													<Fade in={!isPayment}>
														<div
															className={classes.displayBoxContent}
															style={{
																cursor: isOrderFetched ? "pointer" : "default",
															}}
															onClick={
																isOrderFetched && !isPayment
																	? () => {
																			this.setState({
																				isPayment: true,
																			});
																	  }
																	: null
															}
															// style={{ width: "67%" }}
														>
															<div
																className={classes.displayBoxContentMain}
																style={{ width: displayBoxContentMainCollapse }}
															>
																<TitleDesc
																	title="Payment type"
																	desc={
																		!isEmpty(pMethod)
																			? this.props.business.detail.payment_method.find(
																					f => f.id == pMethod
																			  ).name
																			: "No payment selected"
																	}
																/>
																{pMethod == 1 && !isEmpty(bank) ? (
																	<TitleDesc
																		title="Payment type"
																		desc={fpx.find(f => f.code == bank).name}
																	/>
																) : null}
															</div>
															<div className={classes.editButtonContainer}>
																<img src={ExpandMoreIcon} style={{ width: "15px" }} />
															</div>
														</div>
													</Fade>
												)}
											</div>
											{/* <Collapse style={{ width: "100%" }} in={isOrderFetched}>
                        <div
                          className={classes.displayBox}
                          style={{ marginBottom: 0, borderBottom: "none" }}
                        >
                          <Typography className={classes.sectionTitle}>
                            Payment
                          </Typography>
                          <div className={classes.displayBoxContent}>
                            <BookingBank
                              onChange={this.handlePrebookChange}
                              {...this.state}
                            />
                          </div>
                        </div>
                      </Collapse> */}
										</>
									)}
								</div>
							</div>
						</Fade>

						<Slide
							direction="up"
							//in={isWhatsapp || (isShowCard && !isAddress && !isCustomer)}
							in={isOrderFetched}
						>
							<div
								id="cartCard"
								style={{
									position: "fixed",
									bottom: 0,
									zIndex: 150,
									backgroundColor: "rgba(255,255,255,0.7)",
									//opacity: 0.5,
									width: "100%",
									maxWidth: "605px",
									display: "flex",
									flexDirection: "column",
									alignItems: "center",
									//boxShadow: "0px -2px 2px rgba(0, 0, 0, 0.1)",
									// borderRadius: "1rem 1rem 0px 0px",
									borderTop: "1px solid gainsboro",
									boxSizing: "border-box",
									padding: "1.5rem 1.5rem 3.5rem",
								}}
							>
								<PriceList end title="Total" price={bookDetails.grand_total} />

								<div
									style={{
										display: "flex",
										flexDirection: "row",
										justifyContent: "space-between",
										alignItems: "center",
										width: "100%",
									}}
								>
									<Button
										disabled={!isWhatsapp && isOneActive}
										onClick={this.handleSubmit}
										variant="contained"
										color={isWhatsapp ? "primary" : "secondary"}
										style={{ flex: 9 }}
									>
										{isWhatsapp ? "Send message" : "Pay"}
									</Button>
								</div>
								<StoreUpTag />
							</div>
						</Slide>
					</div>
				);
			} else {
				return <NotFound />;
			}
		} else {
			return null;
		}
	}
}

const mapStateToProps = state => ({
	item: state.item,
	business: state.business,
	customer: state.customer,
	booking: state.booking,
	item: state.item,
	cart: state.cart,
	// loading: state.loading,
	notification: state.notification,
	error: state.error,
});

export default connect(mapStateToProps, {
	customNotification,
	clearNotification,
	setInputError,
	getOrderBySessionID,
	getBusiness,
	setItemOfOrder,
	editOrderCustomer,
	editOrderAddress,
	editOrderNotes,
	getBookDetails,
	getPaymentList,
	setCartItem,
	getShippingCatalog,

	completeOrder,
	createLead,
	setLoading,
})(withStyles(styles)(withMobileDialog({ breakpoint: "xs" })(withWidth()(withRouter(Checkout)))));
