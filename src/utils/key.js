import axios from "axios";
axios.defaults.headers.common["X-Data-Source"] = process.env.NODE_ENV !== "development" ? "live" : "test";

export const XANO_APILINK =
	process.env.NODE_ENV === "production"
		? "https://api.storeup.io/api:BOIrhKJQ"
		: "https://api.storeup.io/api:BOIrhKJQ:dev";

export const APILINK =
	process.env.NODE_ENV === "production" ? "https://api.pinjam.co/dbot" : "http://192.168.4.12:3000/dbot";
