export const mainBgColor = "#fff";

export const primaryLight = "#6bff96";
export const primary = "#25d366";
export const primaryDark = "#00a038";

export const secondaryLight = "#0047b0";
export const secondary = "#0071e3";
export const secondaryDark = "#629fff";

export const greywhite = "gainsboro";
export const greylight = "darkgrey";
export const grey = "grey";
export const greydark = "dimgrey";
export const black = "#252525";

export const successColor = "#206166";
export const errorColor = "firebrick";
