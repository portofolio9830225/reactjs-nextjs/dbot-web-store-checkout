import React, { Component } from "react";
import { Switch } from "react-router-dom";
import PublicRoute from "./components/layout/PublicRoute";

import PageNotFound from "./components/NotFound";
import Checkout from "./pages/Checkout";

class App extends Component {
  render() {
    return (
      <div id="HOC">
        <Switch>
          <PublicRoute exact path="/" component={Checkout} />

          <PublicRoute component={PageNotFound} />
        </Switch>
      </div>
    );
  }
}

export default App;
